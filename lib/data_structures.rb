# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  # your code goes here
  arr.max - arr.min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  # your code goes here
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  # your code goes here
  str.downcase.count('aeiou')
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  # your code goes here
  str.downcase.delete('aeiou')
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  # your code goes here
  int.to_s.split("").sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  # your code goes here
  #Was thinking this would be a clever way of doing it, but failed to realize it wouldn't work if the string has all the same character
  #So I created a new version below, although it is a code sniff, I think there may be a clearner way, going to look into it
  #str.downcase.count(str.downcase) > str.length
  str.downcase > str.downcase.split("").uniq.join
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  # your code goes here
  "(#{arr[0..2].join}) #{arr[3..5].join}-#{arr[6..9].join}"
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  # your code goes here
  arr = str.split(",")
  arr.max.to_i - arr.min.to_i
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!

#But, I did try all cases in repl, even the cases not tested with Rspec, as I was frustrated it wasn't all incompassing
#My understanding was that if offset was larger than the length of the array or smaller than the length of the array * -1
#then it would be treated as my_rotate(-1) and my_rotate(arr.length * -1) respectively but after closer examination
#I realized that it actually modulos the offset, so I fixed my code to do that and it is now equivalent to rotate for all numbers

def my_rotate(arr, offset=1)
  # your code goes here
  offset = offset % arr.length if offset > arr.length || offset < arr.length * -1

  if offset == 0
    arr
  elsif offset > 0
    arr.drop(offset) + arr.take(offset)
  elsif offset < 0
    arr[offset..-1] + arr.take(offset + arr.length)
  end
end
